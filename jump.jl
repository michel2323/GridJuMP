using JuMP
using Ipopt
gpm = Model(solver=IpoptSolver())
@variable(gpm, LL_1_1_1, lowerbound =        0, upperbound =        1)
@variable(gpm, LL_1_2_2_0, lowerbound =        0, upperbound =        1)

@variable(gpm, LL_1_2_2_1, lowerbound =        0, upperbound =        1)

@variable(gpm, LL_1_2_3_0, lowerbound =        0, upperbound =        1)

@variable(gpm, LL_1_2_3_1, lowerbound =        0, upperbound =        1)

@variable(gpm, LL_2_1_1, lowerbound =        0, upperbound =        1)
@variable(gpm, LL_2_2_2_0, lowerbound =        0, upperbound =        1)

@variable(gpm, LL_2_2_2_1, lowerbound =        0, upperbound =        1)

@variable(gpm, LL_2_2_3_0, lowerbound =        0, upperbound =        1)

@variable(gpm, LL_2_2_3_1, lowerbound =        0, upperbound =        1)

@variable(gpm, Pg_1_1_1_1, lowerbound =        0, upperbound =     3.18)

@variable(gpm, Pg_1_1_2_2_0, lowerbound =        0, upperbound =     3.18)

@variable(gpm, Pg_1_1_2_2_1, lowerbound =        0, upperbound =     3.18)

@variable(gpm, Pg_1_1_2_3_0, lowerbound =        0, upperbound =     3.18)

@variable(gpm, Pg_1_1_2_3_1, lowerbound =        0, upperbound =     3.18)

@variable(gpm, Qg_1_1_1_1, lowerbound =       -2, upperbound =        2)

@variable(gpm, Qg_1_1_2_2_0, lowerbound =       -2, upperbound =        2)

@variable(gpm, Qg_1_1_2_2_1, lowerbound =       -2, upperbound =        2)

@variable(gpm, Qg_1_1_2_3_0, lowerbound =       -2, upperbound =        2)

@variable(gpm, Qg_1_1_2_3_1, lowerbound =       -2, upperbound =        2)

@variable(gpm, Vi_1_1_1)
@variable(gpm, Vi_1_2_2_0)
@variable(gpm, Vi_1_2_2_1)
@variable(gpm, Vi_1_2_3_0)
@variable(gpm, Vi_1_2_3_1)
@variable(gpm, Vi_2_1_1)
@variable(gpm, Vi_2_2_2_0)
@variable(gpm, Vi_2_2_2_1)
@variable(gpm, Vi_2_2_3_0)
@variable(gpm, Vi_2_2_3_1)
@variable(gpm, Vr_1_1_1)
@variable(gpm, Vr_1_2_2_0)
@variable(gpm, Vr_1_2_2_1)
@variable(gpm, Vr_1_2_3_0)
@variable(gpm, Vr_1_2_3_1)
@variable(gpm, Vr_2_1_1)
@variable(gpm, Vr_2_2_2_0)
@variable(gpm, Vr_2_2_2_1)
@variable(gpm, Vr_2_2_3_0)
@variable(gpm, Vr_2_2_3_1)
@variable(gpm, WC_1_1_1, lowerbound =        0, upperbound =      0.2)
@variable(gpm, WC_1_2_2_0, lowerbound =        0, upperbound =     0.25)

@variable(gpm, WC_1_2_2_1, lowerbound =        0, upperbound =      0.2)

@variable(gpm, WC_1_2_3_0, lowerbound =        0, upperbound =      0.3)

@variable(gpm, WC_1_2_3_1, lowerbound =        0, upperbound =     0.15)

@variable(gpm, WC_2_1_1, lowerbound =        0, upperbound =      0.1)
@variable(gpm, WC_2_2_2_0, lowerbound =        0, upperbound =     0.15)

@variable(gpm, WC_2_2_2_1, lowerbound =        0, upperbound =      0.1)

@variable(gpm, WC_2_2_3_0, lowerbound =        0, upperbound =     0.15)

@variable(gpm, WC_2_2_3_1, lowerbound =        0, upperbound =     0.05)

setvalue(LL_1_1_1,        0)
setvalue(LL_1_2_2_0,        0)
setvalue(LL_1_2_2_1,        0)
setvalue(LL_1_2_3_0,        0)
setvalue(LL_1_2_3_1,        0)
setvalue(LL_2_1_1,        0)
setvalue(LL_2_2_2_0,        0)
setvalue(LL_2_2_2_1,        0)
setvalue(LL_2_2_3_0,        0)
setvalue(LL_2_2_3_1,        0)
setvalue(Pg_1_1_1_1,    2.244)
setvalue(Pg_1_1_2_2_0,    2.244)
setvalue(Pg_1_1_2_2_1,    2.244)
setvalue(Pg_1_1_2_3_0,    2.244)
setvalue(Pg_1_1_2_3_1,    2.244)
setvalue(Qg_1_1_1_1,    1.485)
setvalue(Qg_1_1_2_2_0,    1.485)
setvalue(Qg_1_1_2_2_1,    1.485)
setvalue(Qg_1_1_2_3_0,    1.485)
setvalue(Qg_1_1_2_3_1,    1.485)
setvalue(Vi_1_1_1,        0)
setvalue(Vi_1_2_2_0,        0)
setvalue(Vi_1_2_2_1,        0)
setvalue(Vi_1_2_3_0,        0)
setvalue(Vi_1_2_3_1,        0)
setvalue(Vi_2_1_1,        0)
setvalue(Vi_2_2_2_0,        0)
setvalue(Vi_2_2_2_1,        0)
setvalue(Vi_2_2_3_0,        0)
setvalue(Vi_2_2_3_1,        0)
setvalue(Vr_1_1_1,     1.02)
setvalue(Vr_1_2_2_0,     1.02)
setvalue(Vr_1_2_2_1,     1.02)
setvalue(Vr_1_2_3_0,     1.02)
setvalue(Vr_1_2_3_1,     1.02)
setvalue(Vr_2_1_1,    0.945)
setvalue(Vr_2_2_2_0,    0.945)
setvalue(Vr_2_2_2_1,    0.945)
setvalue(Vr_2_2_3_0,    0.945)
setvalue(Vr_2_2_3_1,    0.945)
setvalue(WC_1_1_1,        0)
setvalue(WC_1_2_2_0,        0)
setvalue(WC_1_2_2_1,        0)
setvalue(WC_1_2_3_0,        0)
setvalue(WC_1_2_3_1,        0)
setvalue(WC_2_1_1,        0)
setvalue(WC_2_2_2_0,        0)
setvalue(WC_2_2_2_1,        0)
setvalue(WC_2_2_3_0,        0)
setvalue(WC_2_2_3_1,        0)
@NLconstraint(gpm, Vr_1_1_1^2 + Vi_1_1_1^2 >= 0.81)
@NLconstraint(gpm, Vr_1_1_1^2 + Vi_1_1_1^2 <= 1.21)
@NLconstraint(gpm, Pg_1_1_1_1 - 2.24409 <= 0.0166667)
@NLconstraint(gpm, Pg_1_1_1_1 - 2.24409 >= -0.0166667)
@NLconstraint(gpm, -(3.86688 * (Vr_1_1_1 * Vr_1_1_1 + Vi_1_1_1 * Vi_1_1_1) 
- 3.81563 * (Vr_1_1_1 * Vr_2_1_1 + Vi_1_1_1 * Vi_2_1_1) + -19.0781 * (Vr_1_1_1 
* Vi_2_1_1 - Vi_1_1_1 * Vr_2_1_1)) + Pg_1_1_1_1 + 0.2 - 0.51 * (1 - LL_1_1_1) 
- WC_1_1_1 == 0)
@NLconstraint(gpm, -(-(-19.0781 * (Vr_1_1_1 * Vr_1_1_1 + Vi_1_1_1 * Vi_1_1_1)) 
+ -19.0781 * (Vr_1_1_1 * Vr_2_1_1 + Vi_1_1_1 * Vi_2_1_1) + 3.81563 * (Vr_1_1_1 
* Vi_2_1_1 - Vi_1_1_1 * Vr_2_1_1)) + Qg_1_1_1_1 - 0.3199 * (1 - LL_1_1_1) 
== 0)
@NLconstraint(gpm, Vr_1_2_2_0^2 + Vi_1_2_2_0^2 >= 0.81)
@NLconstraint(gpm, Vr_1_2_2_0^2 + Vi_1_2_2_0^2 <= 1.21)
@NLconstraint(gpm, Pg_1_1_2_2_0 - Pg_1_1_1_1 <= 0.0166667)
@NLconstraint(gpm, Pg_1_1_2_2_0 - Pg_1_1_1_1 >= -0.0166667)
@NLconstraint(gpm, -(3.86688 * (Vr_1_2_2_0 * Vr_1_2_2_0 + Vi_1_2_2_0 * Vi_1_2_2_0) 
- 3.81563 * (Vr_1_2_2_0 * Vr_2_2_2_0 + Vi_1_2_2_0 * Vi_2_2_2_0) + -19.0781 
* (Vr_1_2_2_0 * Vi_2_2_2_0 - Vi_1_2_2_0 * Vr_2_2_2_0)) + Pg_1_1_2_2_0 + 0.25 
- 0.52 * (1 - LL_1_2_2_0) - WC_1_2_2_0 == 0)
@NLconstraint(gpm, -(-(-19.0781 * (Vr_1_2_2_0 * Vr_1_2_2_0 + Vi_1_2_2_0 * 
Vi_1_2_2_0)) + -19.0781 * (Vr_1_2_2_0 * Vr_2_2_2_0 + Vi_1_2_2_0 * Vi_2_2_2_0) 
+ 3.81563 * (Vr_1_2_2_0 * Vi_2_2_2_0 - Vi_1_2_2_0 * Vr_2_2_2_0)) + Qg_1_1_2_2_0 
- 0.3299 * (1 - LL_1_2_2_0) == 0)
@NLconstraint(gpm, Vr_1_2_2_1^2 + Vi_1_2_2_1^2 >= 0.81)
@NLconstraint(gpm, Vr_1_2_2_1^2 + Vi_1_2_2_1^2 <= 1.21)
@NLconstraint(gpm, Pg_1_1_2_2_1 - Pg_1_1_1_1 <= 0.0166667)
@NLconstraint(gpm, Pg_1_1_2_2_1 - Pg_1_1_1_1 >= -0.0166667)
@NLconstraint(gpm, -(3.86688 * (Vr_1_2_2_1 * Vr_1_2_2_1 + Vi_1_2_2_1 * Vi_1_2_2_1) 
- 3.81563 * (Vr_1_2_2_1 * Vr_2_2_2_1 + Vi_1_2_2_1 * Vi_2_2_2_1) + -19.0781 
* (Vr_1_2_2_1 * Vi_2_2_2_1 - Vi_1_2_2_1 * Vr_2_2_2_1)) + Pg_1_1_2_2_1 + 0.2 
- 0.5 * (1 - LL_1_2_2_1) - WC_1_2_2_1 == 0)
@NLconstraint(gpm, -(-(-19.0781 * (Vr_1_2_2_1 * Vr_1_2_2_1 + Vi_1_2_2_1 * 
Vi_1_2_2_1)) + -19.0781 * (Vr_1_2_2_1 * Vr_2_2_2_1 + Vi_1_2_2_1 * Vi_2_2_2_1) 
+ 3.81563 * (Vr_1_2_2_1 * Vi_2_2_2_1 - Vi_1_2_2_1 * Vr_2_2_2_1)) + Qg_1_1_2_2_1 
- 0.3299 * (1 - LL_1_2_2_1) == 0)
@NLconstraint(gpm, Vr_1_2_3_0^2 + Vi_1_2_3_0^2 >= 0.81)
@NLconstraint(gpm, Vr_1_2_3_0^2 + Vi_1_2_3_0^2 <= 1.21)
@NLconstraint(gpm, Pg_1_1_2_3_0 - Pg_1_1_2_2_0 <= 0.0166667)
@NLconstraint(gpm, Pg_1_1_2_3_0 - Pg_1_1_2_2_0 >= -0.0166667)
@NLconstraint(gpm, -(3.86688 * (Vr_1_2_3_0 * Vr_1_2_3_0 + Vi_1_2_3_0 * Vi_1_2_3_0) 
- 3.81563 * (Vr_1_2_3_0 * Vr_2_2_3_0 + Vi_1_2_3_0 * Vi_2_2_3_0) + -19.0781 
* (Vr_1_2_3_0 * Vi_2_2_3_0 - Vi_1_2_3_0 * Vr_2_2_3_0)) + Pg_1_1_2_3_0 + 0.3 
- 0.53 * (1 - LL_1_2_3_0) - WC_1_2_3_0 == 0)
@NLconstraint(gpm, -(-(-19.0781 * (Vr_1_2_3_0 * Vr_1_2_3_0 + Vi_1_2_3_0 * 
Vi_1_2_3_0)) + -19.0781 * (Vr_1_2_3_0 * Vr_2_2_3_0 + Vi_1_2_3_0 * Vi_2_2_3_0) 
+ 3.81563 * (Vr_1_2_3_0 * Vi_2_2_3_0 - Vi_1_2_3_0 * Vr_2_2_3_0)) + Qg_1_1_2_3_0 
- 0.3399 * (1 - LL_1_2_3_0) == 0)
@NLconstraint(gpm, Vr_1_2_3_1^2 + Vi_1_2_3_1^2 >= 0.81)
@NLconstraint(gpm, Vr_1_2_3_1^2 + Vi_1_2_3_1^2 <= 1.21)
@NLconstraint(gpm, Pg_1_1_2_3_1 - Pg_1_1_2_2_1 <= 0.0166667)
@NLconstraint(gpm, Pg_1_1_2_3_1 - Pg_1_1_2_2_1 >= -0.0166667)
@NLconstraint(gpm, -(3.86688 * (Vr_1_2_3_1 * Vr_1_2_3_1 + Vi_1_2_3_1 * Vi_1_2_3_1) 
- 3.81563 * (Vr_1_2_3_1 * Vr_2_2_3_1 + Vi_1_2_3_1 * Vi_2_2_3_1) + -19.0781 
* (Vr_1_2_3_1 * Vi_2_2_3_1 - Vi_1_2_3_1 * Vr_2_2_3_1)) + Pg_1_1_2_3_1 + 0.15 
- 0.49 * (1 - LL_1_2_3_1) - WC_1_2_3_1 == 0)
@NLconstraint(gpm, -(-(-19.0781 * (Vr_1_2_3_1 * Vr_1_2_3_1 + Vi_1_2_3_1 * 
Vi_1_2_3_1)) + -19.0781 * (Vr_1_2_3_1 * Vr_2_2_3_1 + Vi_1_2_3_1 * Vi_2_2_3_1) 
+ 3.81563 * (Vr_1_2_3_1 * Vi_2_2_3_1 - Vi_1_2_3_1 * Vr_2_2_3_1)) + Qg_1_1_2_3_1 
- 0.3399 * (1 - LL_1_2_3_1) == 0)
@NLconstraint(gpm, Vr_2_1_1^2 + Vi_2_1_1^2 >= 0.81)
@NLconstraint(gpm, Vr_2_1_1^2 + Vi_2_1_1^2 <= 1.21)
@NLconstraint(gpm, -(3.86688 * (Vr_2_1_1 * Vr_2_1_1 + Vi_2_1_1 * Vi_2_1_1) 
- 3.81563 * (Vr_2_1_1 * Vr_1_1_1 + Vi_2_1_1 * Vi_1_1_1) + -19.0781 * (Vr_2_1_1 
* Vi_1_1_1 - Vi_2_1_1 * Vr_1_1_1)) + 0.1 - 1.71 * (1 - LL_2_1_1) - WC_2_1_1 
== 0)
@NLconstraint(gpm, -(-(-19.0781 * (Vr_2_1_1 * Vr_2_1_1 + Vi_2_1_1 * Vi_2_1_1)) 
+ -19.0781 * (Vr_2_1_1 * Vr_1_1_1 + Vi_2_1_1 * Vi_1_1_1) + 3.81563 * (Vr_2_1_1 
* Vi_1_1_1 - Vi_2_1_1 * Vr_1_1_1)) - 1.0535 * (1 - LL_2_1_1) == 0)
@NLconstraint(gpm, Vr_2_2_2_0^2 + Vi_2_2_2_0^2 >= 0.81)
@NLconstraint(gpm, Vr_2_2_2_0^2 + Vi_2_2_2_0^2 <= 1.21)
@NLconstraint(gpm, -(3.86688 * (Vr_2_2_2_0 * Vr_2_2_2_0 + Vi_2_2_2_0 * Vi_2_2_2_0) 
- 3.81563 * (Vr_2_2_2_0 * Vr_1_2_2_0 + Vi_2_2_2_0 * Vi_1_2_2_0) + -19.0781 
* (Vr_2_2_2_0 * Vi_1_2_2_0 - Vi_2_2_2_0 * Vr_1_2_2_0)) + 0.15 - 1.72 * (1 
- LL_2_2_2_0) - WC_2_2_2_0 == 0)
@NLconstraint(gpm, -(-(-19.0781 * (Vr_2_2_2_0 * Vr_2_2_2_0 + Vi_2_2_2_0 * 
Vi_2_2_2_0)) + -19.0781 * (Vr_2_2_2_0 * Vr_1_2_2_0 + Vi_2_2_2_0 * Vi_1_2_2_0) 
+ 3.81563 * (Vr_2_2_2_0 * Vi_1_2_2_0 - Vi_2_2_2_0 * Vr_1_2_2_0)) - 1.0535 
* (1 - LL_2_2_2_0) == 0)
@NLconstraint(gpm, Vr_2_2_2_1^2 + Vi_2_2_2_1^2 >= 0.81)
@NLconstraint(gpm, Vr_2_2_2_1^2 + Vi_2_2_2_1^2 <= 1.21)
@NLconstraint(gpm, -(3.86688 * (Vr_2_2_2_1 * Vr_2_2_2_1 + Vi_2_2_2_1 * Vi_2_2_2_1) 
- 3.81563 * (Vr_2_2_2_1 * Vr_1_2_2_1 + Vi_2_2_2_1 * Vi_1_2_2_1) + -19.0781 
* (Vr_2_2_2_1 * Vi_1_2_2_1 - Vi_2_2_2_1 * Vr_1_2_2_1)) + 0.1 - 1.7 * (1 - 
LL_2_2_2_1) - WC_2_2_2_1 == 0)
@NLconstraint(gpm, -(-(-19.0781 * (Vr_2_2_2_1 * Vr_2_2_2_1 + Vi_2_2_2_1 * 
Vi_2_2_2_1)) + -19.0781 * (Vr_2_2_2_1 * Vr_1_2_2_1 + Vi_2_2_2_1 * Vi_1_2_2_1) 
+ 3.81563 * (Vr_2_2_2_1 * Vi_1_2_2_1 - Vi_2_2_2_1 * Vr_1_2_2_1)) - 1.0535 
* (1 - LL_2_2_2_1) == 0)
@NLconstraint(gpm, Vr_2_2_3_0^2 + Vi_2_2_3_0^2 >= 0.81)
@NLconstraint(gpm, Vr_2_2_3_0^2 + Vi_2_2_3_0^2 <= 1.21)
@NLconstraint(gpm, -(3.86688 * (Vr_2_2_3_0 * Vr_2_2_3_0 + Vi_2_2_3_0 * Vi_2_2_3_0) 
- 3.81563 * (Vr_2_2_3_0 * Vr_1_2_3_0 + Vi_2_2_3_0 * Vi_1_2_3_0) + -19.0781 
* (Vr_2_2_3_0 * Vi_1_2_3_0 - Vi_2_2_3_0 * Vr_1_2_3_0)) + 0.15 - 1.73 * (1 
- LL_2_2_3_0) - WC_2_2_3_0 == 0)
@NLconstraint(gpm, -(-(-19.0781 * (Vr_2_2_3_0 * Vr_2_2_3_0 + Vi_2_2_3_0 * 
Vi_2_2_3_0)) + -19.0781 * (Vr_2_2_3_0 * Vr_1_2_3_0 + Vi_2_2_3_0 * Vi_1_2_3_0) 
+ 3.81563 * (Vr_2_2_3_0 * Vi_1_2_3_0 - Vi_2_2_3_0 * Vr_1_2_3_0)) - 1.0535 
* (1 - LL_2_2_3_0) == 0)
@NLconstraint(gpm, Vr_2_2_3_1^2 + Vi_2_2_3_1^2 >= 0.81)
@NLconstraint(gpm, Vr_2_2_3_1^2 + Vi_2_2_3_1^2 <= 1.21)
@NLconstraint(gpm, -(3.86688 * (Vr_2_2_3_1 * Vr_2_2_3_1 + Vi_2_2_3_1 * Vi_2_2_3_1) 
- 3.81563 * (Vr_2_2_3_1 * Vr_1_2_3_1 + Vi_2_2_3_1 * Vi_1_2_3_1) + -19.0781 
* (Vr_2_2_3_1 * Vi_1_2_3_1 - Vi_2_2_3_1 * Vr_1_2_3_1)) + 0.05 - 1.69 * (1 
- LL_2_2_3_1) - WC_2_2_3_1 == 0)
@NLconstraint(gpm, -(-(-19.0781 * (Vr_2_2_3_1 * Vr_2_2_3_1 + Vi_2_2_3_1 * 
Vi_2_2_3_1)) + -19.0781 * (Vr_2_2_3_1 * Vr_1_2_3_1 + Vi_2_2_3_1 * Vi_1_2_3_1) 
+ 3.81563 * (Vr_2_2_3_1 * Vi_1_2_3_1 - Vi_2_2_3_1 * Vr_1_2_3_1)) - 1.0535 
* (1 - LL_2_2_3_1) == 0)
@objective(gpm, :Min, 1000 + 80 * Pg_1_1_1_1 * 100 + 0.1 * (Pg_1_1_1_1 * 
100) ^ 2 + 10000 * LL_1_1_1 * 51 + 20 * WC_1_1_1 + (1000 + 80 * Pg_1_1_2_2_0 
* 100 + 0.1 * (Pg_1_1_2_2_0 * 100) ^ 2) * 0.25 + (1000 + 80 * Pg_1_1_2_2_1 
* 100 + 0.1 * (Pg_1_1_2_2_1 * 100) ^ 2) * 0.75 + (1000 + 80 * Pg_1_1_2_3_0 
* 100 + 0.1 * (Pg_1_1_2_3_0 * 100) ^ 2) * 0.25 + (1000 + 80 * Pg_1_1_2_3_1 
* 100 + 0.1 * (Pg_1_1_2_3_1 * 100) ^ 2) * 0.75 + 10000 * LL_1_2_2_0 * 0.25 
* 52 + 10000 * LL_1_2_2_1 * 0.75 * 50 + 10000 * LL_1_2_3_0 * 0.25 * 53 + 
10000 * LL_1_2_3_1 * 0.75 * 49 + 20 * WC_1_2_2_0 * 0.25 + 20 * WC_1_2_2_1 
* 0.75 + 20 * WC_1_2_3_0 * 0.25 + 20 * WC_1_2_3_1 * 0.75 + 10000 * LL_2_1_1 
* 171 + 20 * WC_2_1_1 + 10000 * LL_2_2_2_0 * 0.25 * 172 + 10000 * LL_2_2_2_1 
* 0.75 * 170 + 10000 * LL_2_2_3_0 * 0.25 * 173 + 10000 * LL_2_2_3_1 * 0.75 
* 169 + 20 * WC_2_2_2_0 * 0.25 + 20 * WC_2_2_2_1 * 0.75 + 20 * WC_2_2_3_0 
* 0.25 + 20 * WC_2_2_3_1 * 0.75)
print(gpm)
status = solve(gpm)
println("Objective value: ", getobjectivevalue(gpm))
println("LL_1_1_1 value: ",getvalue(LL_1_1_1))
println("LL_1_2_2_0 value: ",getvalue(LL_1_2_2_0))
println("LL_1_2_2_1 value: ",getvalue(LL_1_2_2_1))
println("LL_1_2_3_0 value: ",getvalue(LL_1_2_3_0))
println("LL_1_2_3_1 value: ",getvalue(LL_1_2_3_1))
println("LL_2_1_1 value: ",getvalue(LL_2_1_1))
println("LL_2_2_2_0 value: ",getvalue(LL_2_2_2_0))
println("LL_2_2_2_1 value: ",getvalue(LL_2_2_2_1))
println("LL_2_2_3_0 value: ",getvalue(LL_2_2_3_0))
println("LL_2_2_3_1 value: ",getvalue(LL_2_2_3_1))
println("Pg_1_1_1_1 value: ",getvalue(Pg_1_1_1_1))
println("Pg_1_1_2_2_0 value: ",getvalue(Pg_1_1_2_2_0))
println("Pg_1_1_2_2_1 value: ",getvalue(Pg_1_1_2_2_1))
println("Pg_1_1_2_3_0 value: ",getvalue(Pg_1_1_2_3_0))
println("Pg_1_1_2_3_1 value: ",getvalue(Pg_1_1_2_3_1))
println("Qg_1_1_1_1 value: ",getvalue(Qg_1_1_1_1))
println("Qg_1_1_2_2_0 value: ",getvalue(Qg_1_1_2_2_0))
println("Qg_1_1_2_2_1 value: ",getvalue(Qg_1_1_2_2_1))
println("Qg_1_1_2_3_0 value: ",getvalue(Qg_1_1_2_3_0))
println("Qg_1_1_2_3_1 value: ",getvalue(Qg_1_1_2_3_1))
println("Vi_1_1_1 value: ",getvalue(Vi_1_1_1))
println("Vi_1_2_2_0 value: ",getvalue(Vi_1_2_2_0))
println("Vi_1_2_2_1 value: ",getvalue(Vi_1_2_2_1))
println("Vi_1_2_3_0 value: ",getvalue(Vi_1_2_3_0))
println("Vi_1_2_3_1 value: ",getvalue(Vi_1_2_3_1))
println("Vi_2_1_1 value: ",getvalue(Vi_2_1_1))
println("Vi_2_2_2_0 value: ",getvalue(Vi_2_2_2_0))
println("Vi_2_2_2_1 value: ",getvalue(Vi_2_2_2_1))
println("Vi_2_2_3_0 value: ",getvalue(Vi_2_2_3_0))
println("Vi_2_2_3_1 value: ",getvalue(Vi_2_2_3_1))
println("Vr_1_1_1 value: ",getvalue(Vr_1_1_1))
println("Vr_1_2_2_0 value: ",getvalue(Vr_1_2_2_0))
println("Vr_1_2_2_1 value: ",getvalue(Vr_1_2_2_1))
println("Vr_1_2_3_0 value: ",getvalue(Vr_1_2_3_0))
println("Vr_1_2_3_1 value: ",getvalue(Vr_1_2_3_1))
println("Vr_2_1_1 value: ",getvalue(Vr_2_1_1))
println("Vr_2_2_2_0 value: ",getvalue(Vr_2_2_2_0))
println("Vr_2_2_2_1 value: ",getvalue(Vr_2_2_2_1))
println("Vr_2_2_3_0 value: ",getvalue(Vr_2_2_3_0))
println("Vr_2_2_3_1 value: ",getvalue(Vr_2_2_3_1))
println("WC_1_1_1 value: ",getvalue(WC_1_1_1))
println("WC_1_2_2_0 value: ",getvalue(WC_1_2_2_0))
println("WC_1_2_2_1 value: ",getvalue(WC_1_2_2_1))
println("WC_1_2_3_0 value: ",getvalue(WC_1_2_3_0))
println("WC_1_2_3_1 value: ",getvalue(WC_1_2_3_1))
println("WC_2_1_1 value: ",getvalue(WC_2_1_1))
println("WC_2_2_2_0 value: ",getvalue(WC_2_2_2_0))
println("WC_2_2_2_1 value: ",getvalue(WC_2_2_2_1))
println("WC_2_2_3_0 value: ",getvalue(WC_2_2_3_0))
println("WC_2_2_3_1 value: ",getvalue(WC_2_2_3_1))
