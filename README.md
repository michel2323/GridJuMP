# Exporting GridPACK models to StructJuMP 


## Goals 

1. Run JuMP model
2. Run same model as a StructJuMP model with one scenario
3. Run the StructJuMP model with duplicated scenarios.